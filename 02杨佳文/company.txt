--先创建一个数据库用来存放某公司的员工信息，数据库的名称为company，包含2个数据文件1个日志
--文件，数据文件和日志文件全部存放在E盘中，初始大小，增长和最大大小自己设定
--   再创建表：
create database company 
go
use company
go
--部门信息表（sectionInfo）
--	部门编号  sectionID  int 标识列  主键
--	部门名称  sectionName  varchar(10)  不能为空
create table sectionInfo(
	sectionID int identity(1,1) primary key ,
	sectionName varchar(10) not null
);
go
--员工信息表（userInfo）
--	员工编号  userNo  int  标识列 主键  不允许为空
--	员工姓名  userName  varchar(10)  唯一约束 不允许为空 长度必须大于4
--	员工性别  userSex   varchar(2)  不允许为空  只能是男或女
--	员工年龄  userAge   int  不能为空  范围在1-100之间
--	员工地址  userAddress  varchar(50)  默认值为“湖北”
--	员工部门  userSection  int  外键，引用部门信息表的部门编号

create table userInfo(
	userNo int identity(1,1) primary key not null,
	userName varchar(10) unique not null check(len(userName)>4),
	userSex varchar(2) not null check(userSex='男'or userSex='女'),
	userAge int not null check(userAge between 1 and 100),
	userAddress varchar(50) default '湖北',
	userSection int foreign key references sectionInfo(sectionId)
	 
);
go
--员工考勤表（workInfo）
--	考勤编号  workId int 标识列  主键  不能为空
--	考勤员工  userId  int 外键 引用员工信息表的员工编号 不能为空
--	考勤时间  workTime datetime 不能为空
--	考勤说明  workDescription  varchar(40) 不能为空 内容只能是“迟到”，“早退”，“旷工”，“病假”，“事假”中的一种
create table workInfo(
	workId int identity primary key not null,
	userId int foreign key references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription varchar(40) not null check(workDescription='迟到'or workDescription='早退'or workDescription='旷工'or workDescription='病假'or workDescription='事假') not null
);