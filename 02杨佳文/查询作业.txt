--------------------
-----简单查询------
--------------------
--(1)select * from 表名
-----------------------
--1.查询所有的班级信息
select * from ClassInfo;
--2.查询所有的学生信息
select * from StudentInfo;
--3.查询所有的课程 CourseInfo 信息
select * from CourseInfo;
--4.查询所有的分数 ScoreInfo 信息
select * from ScoreInfo;
--------------------
--(2)查询前n部分数据
--------------------
--1.查询3条学生 StudentInfo 信息
select top 3 * from StudentInfo;
--2.查询3条课程 CourseInfo 信息
select top 3*from CourseInfo;
--3.查询前50%的学生信息
select top 50 percent *from StudentInfo;
--------------------
--(3)查询指定列&指定别名
--------------------
--1.查询学生信息，只需要查出学生姓名和生日
select StuName,StuBirth from StudentInfo;
--2.将StuBirth用学生姓名替代，StuName用学生姓名替代
select StuName'学生姓名',StuBirth'学生生日'from StudentInfo;
--------------------
--(4)排序:select * from <表名> order by 字段
--------------------
--1.  查询所有的成绩信息，并且按照成绩降序
select * from ScoreInfo order by Score desc;
--2.  查询所有的学生信息，并且按照年龄降序
select * from StudentInfo order by StuBirth desc;
--3.  查询所有学生信息，并且按照出生日期升序
select * from StudentInfo order by StuBirth;
--4.  查询最后5条学生信息
select top 5 *from StudentInfo order by stuId desc;

---------------------------------
------------条件查询------------
---------------------------------
--(1). 单条件查询
---------------------------------
--1.查询软件一班所有的学生；
select * from StudentInfo
where ClassId =1;
--2.查询不是软件一班所有的学生；
select * from StudentInfo
where ClassId!=1;

--3.查询所有的女生；
select * from StudentInfo
where StuGender =0;
--4.查询2000年出生的所有学生；
select * from StudentInfo
where year(StuBirth)=2000;
--5.查询软件1班和3班所有的学生；
select * from StudentInfo
where ClassId=1 or ClassId=3;
--6.查询邮箱没有录入的学生；
select * from StudentInfo
where StuEmail is null;
--7.查询家庭住址不为空的学生；
select * from StudentInfo
where StuAddress is not null;
--8.查询软件1班和2班的学生；
select * from StudentInfo
where ClassId=1 or ClassId=2;
--9.查询钟琪和曾小林的考试成绩；
--10.查询姓名为  刘正、钟琪 、曾小林的学生信息；
select * from StudentInfo
where StuName='钟琪'or stuName='曾小林'or StuName='刘正';
--11.查询不为软件1班和软件2班的学生信息。
select *from StudentInfo
where ClassId!=1 and ClassId!=2;