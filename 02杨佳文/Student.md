-- 建库部分
--------------------
-- 创建学生管理系统数据库，名称为Student
create database Student 
go
use Student
go

--------------------
-- 建表部分
--------------------
-- 创建班级表ClassInfo，存储班级信息，其中字段包含：班级id(ClassId 主键)、班级名称()
create table ClassInfo(
	ClassId int primary key identity(1,1),
	ClassName nvarchar(10) not null
);
go

-- 创建学生表StudentInfo，存储学生信息，其中字段保护：学生id、姓名、性别、手机号、生日、邮箱、家庭住址，所属班级id(外键)
create table StudentInfo(
	StudentId int primary key identity(1,1),
	StudentName nvarchar(10) not null,
	StudentSex nvarchar(1) check(StudentSex='男'or StudentSex='女')default'男'not null,
	StudentPhone varchar(11) check(len(StudentPhone)=11) not null,
	StudentBirth date not null,
	StudentEmail nvarchar(max) not null,
	StudentAddress nvarchar(50) not null,
	ClassId int foreign key references ClassInfo(ClassId)not null
);
go

-- 创建课程表CoureInfo，存储课程信息，其中字段包含：课程id、课程名称、课程学分(2,3,6)
create table CourseInfo(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(20) not null,
	CourseGrade int check(CourseGrade=2 or CourseGrade=3 or CourseGrade=6) not null

);
go
-- 创建班级课程表ClassCourseInfo，存储班级课程信息，其中字段包含：自增id、班级id(外键)、课程id（外键）
create table ClassCourseInfo(
	ClassCourseInfo int primary key identity(1,1),
	ClassId int foreign key references ClassInfo(ClassId) not null,
	CourseId int foreign key references CourseInfo(CourseId) not null
);
go
-- 创建分数表ScoreInfo，存储学生每个课程分数信息，其中字段包含：分数id、学生id(外键)、课程id(外键)、分数
create table  ScoreInfo(
	ScoreId int primary key identity(1,1),
	StudentId int foreign key references StudentInfo(StudentId) not null,
	CourseId int foreign key references CourseInfo(CourseId) not null,
	Score int check(Score between 0 and 100) not null
);
go